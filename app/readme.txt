ZbW Zentrum für berufliche Weiterbildung, Interaktive Medien, 4. Semester, Webtechnologien

Das Screendesign ist unter folgender URL aufrufbar:
https://sketch.cloud/s/3Avw

Folgende Seiten sind unter folgenden URL's aufrufbar:
Startseite: webtech.anderthalb.ch
Festtagstracht: webtech.anderthalb.ch/festtagstracht.html
Kontakt: webtech.anderthalb.ch/kontakt.html

Die Entwicklungsdaten stehen auf Bitbucket zur Vefügung
git clone https://nedom@bitbucket.org/nedom/webtech.semesterarbeit.ch.git